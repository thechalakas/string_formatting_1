﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace String_Formatting_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //here is that {} thing being used to format output

            int a = 20, b = 50;

            //the 0 in bracke maps to a and 1 maps to b
            Console.WriteLine(" value at a is {0} and b is {1}", a, b);

            //now I am going to use the culture info

            int price = 100;
            //displaying without culture info
            Console.WriteLine("The price is {0}", price);
            //displaying with culture info
            //do you see that C - the first argument for tostring - that is the formatting key kind of thing
            Console.WriteLine("The price is {0}", price.ToString("C", new System.Globalization.CultureInfo("en-US")));

            //creating an object that has the custom formatting            
            Water_Bottle bottle = new Water_Bottle(50);
            //now let me use the custom formatting I have used and display
            Console.WriteLine(bottle.ToString("G"));
            Console.WriteLine(bottle.ToString("default"));
            Console.WriteLine(bottle.ToString("minimum"));
            Console.WriteLine(bottle.ToString("long"));
            Console.WriteLine(bottle.ToString("hello"));




            //this is to stop the console from vanishing
            Console.ReadLine();
        }
    }

    //class that will show custom formatting keys for display
    class Water_Bottle
    {
        int quantity;

        public Water_Bottle(int q)
        {
            quantity = q;
        }

        public string ToString(string format)
        {
            //I must first implement a format key called G
            //this is the default tostring behavior.
            //its similar to when you are using a tostring without the string format parameter

            if (string.IsNullOrWhiteSpace(format) || format == "G")
                format = "default";

            //now let me remove 
            switch(format)
            {
                //return a simple string which has a quantity with some cursory words
                case "default": return "quantity is " + quantity;
                //just return the quantity
                case "minimum": return "" + quantity;
                case "long": return "the quantity in the water bottle is - " + quantity;
                default: return "no such format exists";
            }
        }
    }
}

